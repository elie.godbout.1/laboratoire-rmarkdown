### -*-Makefile-*- pour préparer "Programmation lettrée et R Markdown"
##
## Copyright (C) 2021 Vincent Goulet
##
## 'make pdf' crée les fichiers .tex à partir des fichiers .Rnw avec
## Sweave et compile le document maitre avec XeLaTeX.
##
## 'make tex' crée les fichiers .tex à partir des fichiers .Rnw avec
## Sweave.
##
## 'make contrib' crée le fichier COLLABORATEURS.
##
## 'make update-copyright' met à jour l'année de copyright dans toutes
## les sources du document
##
## 'make zip' crée la distribution du matériel pédagogique.
##
## 'make release' crée une nouvelle version dans GitLab et téléverse
## le fichier .zip.
##
## 'make check-url' vérifie la validité de toutes les url présentes
## dans les sources du document.
##
## 'make all' est équivalent à 'make pdf' question d'éviter les
## publications accidentelles.
##
## Auteur: Vincent Goulet
##
## Ce fichier fait partie du projet "Programmation lettrée et R
## Markdown"
## https://gitlab.com/vigou3/laboratoire-rmarkdown

## Principaux fichiers
MASTER = laboratoire-rmarkdown.pdf
ARCHIVE = ${MASTER:.pdf=.zip}
README = README.md
NEWS = NEWS
COLLABORATEURS = COLLABORATEURS
LICENSE = LICENSE

## Autres fichiers à inclure dans l'archive
CONTRIBUTING = CONTRIBUTING.md

## Le document maitre dépend de tous les fichiers .tex autres que
## lui-même.
TEXFILES = $(addsuffix .tex, \
                       $(filter-out $(basename ${MASTER}),\
                                    $(basename $(wildcard *.tex))))

## Fichiers pour les exercices
EXERCISES = $(wildcard exercices/exercice-*.Rmd exercices/exercice-*.md) \
	exercices/analyse-bixi.pdf exercices/analyse-bixi.html \
	exercices/bixi-sample.rds

## Informations de publication extraites du fichier maitre
TITLE = $(shell grep "\\\\title" ${MASTER:.pdf=.tex} \
	| cut -d { -f 2 | tr -d })
REPOSURL = $(shell grep "newcommand{\\\\reposurl" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
YEAR = $(shell grep "newcommand{\\\\year" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
MONTH = $(shell grep "newcommand{\\\\month" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
VERSION = ${YEAR}.${MONTH}

## Auteurs à exclure du fichier COLLABORATEURS (regex)
OMITAUTHORS = Vincent Goulet|Inconnu|unknown

## Outils de travail
TEXI2DVI = LATEX=xelatex TEXINDY=makeindex texi2dvi -b
RENDER_PDF = R -s -e "rmarkdown::render('$<', output_format = 'pdf_document')"
RENDER_HTML = R -s -e "rmarkdown::render('$<', output_format = 'html_document')"
CP = cp -p
RM = rm -rf

## Dossier temporaire pour construire l'archive
BUILDDIR = builddir

## Dépôt GitLab et authentification
REPOSNAME = $(shell basename ${REPOSURL})
APIURL = https://gitlab.com/api/v4/projects/vigou3%2F${REPOSNAME}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

## Variables automatiques
TAGNAME = v${VERSION}


all: pdf

FORCE:

%.pdf: %.Rmd
	${RENDER_PDF}

%.html: %.Rmd
	${RENDER_HTML}

${MASTER}: ${MASTER:.pdf=.tex} ${TEXFILES} ${EXERCISES} $(wildcard images/*)
	${TEXI2DVI} ${MASTER:.pdf=.tex}

${COLLABORATEURS}: FORCE
	(git log --pretty="%an%n" && echo "Laurent Caron") | sort | uniq | \
	  grep -v -E "${OMITAUTHORS}" | \
	  awk 'BEGIN { print "Les personnes dont le nom [1] apparait ci-dessous ont contribué à\nl'\''amélioration de «${TITLE}»." } \
	       { print $$0 } \
	       END { print "\n[1] Noms tels qu'\''ils figurent dans le journal du dépôt Git\n    ${REPOSURL}" }' > ${COLLABORATEURS}

.PHONY: pdf
pdf: ${MASTER}

.PHONY: tex
tex: ${RNWFILES:.Rnw=.tex}

.PHONY: contrib
contrib: ${COLLABORATEURS}

.PHONY: release
release: update-copyright zip check-status upload create-release publish

.PHONY: update-copyright
update-copyright: ${MASTER:.pdf=.tex} ${TEXFILES} ${RFILES}
	for f in $?; \
	    do sed -E '/^(#|%)* +Copyright \(C\)/s/20[0-9]{2}/$(shell date "+%Y")/' \
	           $$f > $$f.tmp && \
	           ${CP} $$f.tmp $$f && \
	           ${RM} $$f.tmp; \
	done

.PHONY: zip
zip: ${MASTER} ${EXERCISES} ${README} ${NEWS} ${LICENSE} ${COLLABORATEURS} ${CONTRIBUTING}
	if [ -d ${BUILDDIR} ]; then ${RM} ${BUILDDIR}; fi
	mkdir ${BUILDDIR}
	touch ${BUILDDIR}/${README} && \
	  awk 'state==0 && /^# / { state=1 }; \
	       /^## Auteur/ { printf("## Édition\n\n%s\n\n", "${VERSION}") } \
	       state' ${README} >> ${BUILDDIR}/${README}
	${CP} ${MASTER} ${EXERCISES} ${NEWS} ${LICENSE} \
	      ${COLLABORATEURS} ${CONTRIBUTING} \
	      ${BUILDDIR}
	cd ${BUILDDIR} && zip --filesync -r ../${ARCHIVE} *
	if [ -e ${COLLABORATEURS} ]; then ${RM} ${COLLABORATEURS}; fi
	${RM} ${BUILDDIR} 

.PHONY: check-status
check-status:
	@echo ----- Checking status of working directory...
	@if [ "master" != $(shell git branch --list | grep ^* | cut -d " " -f 2-) ]; then \
	     echo "not on branch master"; exit 2; fi
	@if [ -n "$(shell git status --porcelain | grep -v '^??')" ]; then \
	     echo "uncommitted changes in repository; not creating release"; exit 2; fi
	@if [ -n "$(shell git log origin/master..HEAD | head -n1)" ]; then \
	    echo "unpushed commits in repository; pushing to origin"; \
	     git push; fi

.PHONY: upload
upload :
	@echo ----- Uploading archive to GitLab...
	$(eval upload_url=$(shell curl --form "file=@${ARCHIVE}" \
	                                        --header "PRIVATE-TOKEN: ${OAUTHTOKEN}"	\
	                                        --silent \
	                                        ${APIURL}/uploads \
	                                   | awk -F '"' '{ print $$8 }'))
	@echo url to file:
	@echo "${upload_url}"
	@echo ----- Done uploading files

.PHONY: create-release
create-release :
	@echo ----- Creating release on GitLab...
	if [ -e relnotes.in ]; then rm relnotes.in; fi
	touch relnotes.in
	awk 'BEGIN { ORS = " "; print "{\"tag_name\": \"${TAGNAME}\"," } \
	      /^$$/ { next } \
	      (state == 0) && /^# / { \
		state = 1; \
		out = $$2; \
	        for(i = 3; i <= NF; i++) { out = out" "$$i }; \
	        printf "\"name\": \"Édition %s\", \"description\":\"", out; \
	        next } \
	      (state == 1) && /^# / { exit } \
	      state == 1 { printf "%s\\n", $$0 } \
	      END { print "\",\"assets\": { \"links\": [{ \"name\": \"${ARCHIVE}\", \"url\": \"${REPOSURL}${upload_url}\", \"link_type\": \"package\" }] }}" }' \
	     ${NEWS} >> relnotes.in
	curl --request POST \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --output /dev/null --silent \
	     "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=master"
	curl --request POST \
	     --data @relnotes.in \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --header "Content-Type: application/json" \
	     --output /dev/null --silent \
	     ${APIURL}/releases
	${RM} relnotes.in
	@echo ----- Done creating the release

.PHONY: publish
publish:
	@echo ----- Publishing the web page...
	git checkout pages && \
	  ${MAKE} && \
	  git checkout master
	@echo ----- Done publishing

.PHONY: check-url
check-url: ${MASTER:.pdf=.tex} ${TEXFILES}
	@echo ----- Checking urls in sources...
	$(eval url=$(shell grep -E -o -h 'https?:\/\/[^./]+(?:\.[^./]+)+(?:\/[^ ]*)?' $? \
		   | cut -d \} -f 1 \
		   | cut -d ] -f 1 \
		   | cut -d '"' -f 1 \
		   | sort | uniq | sed -E "s/.*/\'&\'/"))
	for u in ${url}; \
	    do if curl --output /dev/null --silent --head --fail --max-time 5 $$u; then \
	        echo "URL exists: $$u"; \
	    else \
		echo "URL does not exist (or times out): $$u"; \
	    fi; \
	done

.PHONY: clean
clean:
	${RM} ${MASTER} \
	      ${MASTER:.html=.pdf} \
	      ${ARCHIVE} \
	      ${COLLABORATEURS} \
	      *.aux *.log *.snm *.nav *.vrb
